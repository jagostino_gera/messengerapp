import { Component, HostBinding } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatIconRegistryService } from './shared/services/mat-icon-registry/mat-icon-registry.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'MessengerApp';
  @HostBinding('class.app-root')
  viewClass = true;

  constructor(private translate: TranslateService,
    private materialIconRegistryService: MatIconRegistryService) {

    let clientDefaultLanguage = 'pt-BR';

    translate.setDefaultLang('pt-BR');

    translate.use(clientDefaultLanguage);

    this.materialIconRegistryService.registerSvg();

  }
}

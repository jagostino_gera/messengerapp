import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class MatIconRegistryService {

  constructor(private matIconRegistry: MatIconRegistry, private sanitizer: DomSanitizer) { }
  registerSvg() {
    this.matIconRegistry
    .addSvgIcon('gera', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/images/misc/logo-gera.svg'))
    .addSvgIcon('gera_bag', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/bag.svg'))
    .addSvgIcon('product_box', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/product_box.svg'))
    .addSvgIcon('cream_bottle', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/cream_bottle.svg'))
    .addSvgIcon('gift_icon', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/gift_icon.svg'))
    .addSvgIcon('person', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/person.svg'))
    .addSvgIcon('lamp', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/lamp.svg'))
    .addSvgIcon('export', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/export.svg'))
    .addSvgIcon('upload', this.sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/upload.svg'));
  }
}

import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {
  @HostBinding('class.footer')
  viewClass = true;

  currentYear: string = new Date().getFullYear().toString();

  constructor() { }

  ngOnInit(): void {
  }

}

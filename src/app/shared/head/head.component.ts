import { Component, OnInit, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.less']
})
export class HeadComponent implements OnInit {
  @HostBinding('class.app-head')
  viewClass = true;

  @Input('icon') titleIcon: string;
  @Input('title') titleText: string;

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'number-form-field',
  templateUrl: './number-form-field.component.html',
  styleUrls: ['./number-form-field.component.less']
})
export class NumberFormFieldComponent implements OnInit {
  @HostBinding('class.number-form-field')
  formFieldClass = true;
  constructor() { }

  ngOnInit(): void {
  }

}

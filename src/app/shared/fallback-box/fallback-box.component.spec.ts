import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FallbackBoxComponent } from './fallback-box.component';

describe('FallbackBoxComponent', () => {
  let component: FallbackBoxComponent;
  let fixture: ComponentFixture<FallbackBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FallbackBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FallbackBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

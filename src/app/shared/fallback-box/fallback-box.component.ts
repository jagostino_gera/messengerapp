import { Component, OnInit, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'fallback-box',
  templateUrl: './fallback-box.component.html',
  styleUrls: ['./fallback-box.component.less']
})
export class FallbackBoxComponent implements OnInit {
  @HostBinding('class.fallback-box')
  formFieldClass = true;

  @Input() message: string;

  constructor() { }

  ngOnInit(): void {
  }

}

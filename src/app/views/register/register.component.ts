import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  @HostBinding('class.register')
  viewClass = true;

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit, HostBinding } from '@angular/core';

interface Situation {
  value: string;
  text: string;
}

interface Criticality {
  value: string;
  text: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  @HostBinding('class.home')
  viewClass = true;

  constructor() { }

  ngOnInit(): void {
  }

situations: Situation[] = [
  {value: 'Ativo', text: 'Ativo'},
  {value: 'Inativo', text: 'Inativo'}
]

criticality: Criticality[] = [
  {value: 'Muito alto', text: 'Muito alto'},
  {value: 'Alto', text: 'Alto'},
  {value: 'Média', text: 'Média'},
  {value: 'Baixa', text: 'Baixa'}
]

}
